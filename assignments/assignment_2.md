# Assignment #2

Learn git from: https://git-scm.com/docs/gittutorial

Before beginning with the assignment make sure you registered on http://gitlab.com/

If you have not registered yet, please create an account on http://gitlab.com/


1. Fork this repository on gitlab account: https://gitlab.com/symb-assessment/startbootstrap-bare
2. Add tag to the repository as version *v0.0.1*
3. Create new file 'about_us.html' in the repository, with content
    ```html
    <!DOCTYPE html>
    <html lang="en">
    <head>
      <title>About US</title>
    </head>
      <body>
        <h1>About Us</h1>
      </body>
    </html>
    ``` 
4. Commit the code and push it to the repository
5. Create new branch 'javascript_integration' and checkout the branch
6. Add javascript to the 'about_us.html file', content of 'about_us.html' will look like:
    ```html
    <!DOCTYPE html>
    <html lang="en">
    <head>
      <title>About Us</title>
      <script type="text/javascript">
        function add(a,b){
          return a+b;
        }
      </script>
    </head>
      <body>
        <h1>About Us</h1>
      </body>
    </html>
    ```
7. Checkout on 'master' branch, create new branch 'css_integration' and checkout it
8. Add a folder 'css' in the code
9. Add file 'main_2.css' in the 'css' folder. Commit and push the code
10. Raise merge request for 'css_integration' branch to merge in master
11. Approve the merge request create in point 14. 
12. Tag the master version as 'v0.0.2'
13. Create merge request for 'javascript_integration' to master and approve the same
14. Tag the master version as 'v0.0.3'
15. Push all the changes to the remote repository

